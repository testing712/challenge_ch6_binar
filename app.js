const express = require('express')
const app = express()
const routes = require('./routes/index.route.js')
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger-output.json');
const errorHandler = require('./errorHandler');
const morgan = require('morgan')
const sentry = require('@sentry/node')
const path = require('path')
require('dotenv').config()

sentry.init({
    dsn: process.env.SENTRY_DSN
})


app.use(express.urlencoded({extended: false}))
app.use(express.json())

app.use(express.static(path.join('public','data','uploads')))

app.use((req, res, next) => {
    req.sentry = sentry
    next()
})

app.use(morgan('common'))

app.use(routes)

app.use('/api-docs', swaggerUi.serve);
app.get('/api-docs', swaggerUi.setup(swaggerDocument));

app.use(errorHandler)

module.exports = app
