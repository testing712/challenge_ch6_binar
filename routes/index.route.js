const express = require('express')
const router = express.Router()
const authRoutes = require('./auth.route')
const gameRoutes = require('./game.route')

router.use('/auth', authRoutes)
router.use('/games', gameRoutes)

module.exports = router