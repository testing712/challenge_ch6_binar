const express = require('express')
const router = express.Router()
const dataController = require('../controllers/game.controller')
const jwt = require('jsonwebtoken')
const multer  = require('multer')
const storage = require('../services/multer.service')
const upload = multer(
    { 
        storage: storage,
        fileFilter: (req,file,cb) => {
            if (file.mimetype === 'video/mp4' || file.mimetype === 'video/mkv' || file.mimetype === 'video/avi' || file.mimetype === 'video/webm' ) {
                cb(null,true)
            }else {
                cb(new Error('File type not available'),false)               
            }
        }
    }
)

router.get('/',
(req, res, next) => {
    try {
      if (!req.headers.authorization) {
        throw {
          status: 401,
          message: 'Unauthorized request'
        }
      } else {
        const user = jwt.decode(req.headers.authorization)
        if (user) {
          req.user = user
          next()
        } else {
          throw {
            status: 401,
            message: 'Unauthorized request'
          }
        }
      }
    } catch (err) {
      next(err)
    }
  },
dataController.list)

router.get('/:id',
(req, res, next) => {
    try {
        if (!req.headers.authorization) {
            throw {
                status: 401,
                message: 'Unauthorized request'
            }
        } else {
            const user = jwt.decode(req.headers.authorization)
            if (user) {
                req.user = user
                next()
            } else {
                throw {
                    status: 401,
                    message: 'Unauthorized request'
                }
            }
        }
    } catch (err) {
        next(err)
    }
},
dataController.getById)

router.post('/',
upload.single('upFile'),
// (req, res, next) => 
// {
//     try {
//         if (!req.headers.authorization) {
//             throw {
//                 status: 401,
//                 message: 'Unauthorized request'
//             }
//         } else {
//             const user = jwt.decode(req.headers.authorization)
//             if (user) {
//                 req.user = user
//                 next()
//             } else {
//             throw {
//                 status: 401,
//                 message: 'Unauthorized request'
//                 }
//             }
//         }
//     } catch (err) {
//         next(err)
//     }
// },
dataController.create)

router.put('/:id',
(req, res, next) => {
    try {
        if (!req.headers.authorization) {
            throw {
                status: 401,
                message: 'Unauthorized request'
            }
        } else {
            const user = jwt.decode(req.headers.authorization)
            if (user) {
                req.user = user
                next()
            } else {
            throw {
                status: 401,
                message: 'Unauthorized request'
                }
            }
        }
    } catch (err) {
        next(err)
    }
},
dataController.update)
router.delete('/:id',
(req, res, next) => {
    try {
        if (!req.headers.authorization) {
            throw {
                status: 401,
                message: 'Unauthorized request'
            }
        } else {
            const user = jwt.decode(req.headers.authorization)
            if (user) {
                req.user = user
                next()
            } else {
            throw {
                status: 401,
                message: 'Unauthorized request'
            }
            }
        }
    } catch (err) {
        next(err)
    }
},
dataController.delete)

module.exports = router