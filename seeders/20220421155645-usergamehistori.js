'use strict';

const fs = require('fs')

module.exports = {
  async up (queryInterface, Sequelize) {
    const data = JSON.parse(fs.readFileSync('./masterdata/user-game-histori.json','utf-8'))

    const history = data.map((element) =>{
    return {
      userGameId: element.userGameId,
      topScore: element.topScore,
      loginTime: new Date(),
      logoutTime: new Date(),
      createdAt: new Date(),
      updatedAt: new Date()
    }
  })
  await queryInterface.bulkInsert('UserGameHistories',history , {})
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
  },

  async down (queryInterface, Sequelize) {
    return queryInterface.bulkDelete('UserGameHistories', null, {truncate: true, restartIdentity: true});
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
