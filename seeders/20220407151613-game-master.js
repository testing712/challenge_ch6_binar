'use strict';
const fs = require('fs')

module.exports = {
  async up (queryInterface, Sequelize) {
    const data = JSON.parse(fs.readFileSync('./masterdata/user-games.json','utf-8'))

    const user = data.map((element) =>{
    return {
      username: element.username,
      password: element.password,
      createdAt: new Date(),
      updatedAt: new Date()
    }
  })
  await queryInterface.bulkInsert('UserGames',user , {})
},
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

  async down (queryInterface, Sequelize) {
    return queryInterface.bulkDelete('UserGames', null, {truncate: true, restartIdentity: true});
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
