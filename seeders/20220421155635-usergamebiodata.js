'use strict';

const fs = require('fs')

module.exports = {
  async up (queryInterface, Sequelize) {
    const data = JSON.parse(fs.readFileSync('./masterdata/user-game-biodata.json','utf-8'))

    const biodata = data.map((element) =>{
    return {
      userGameId: element.userGameId,
      nickname: element.nickname,
      email: element.email,
      level: element.level,
      decription: element.decription,
      createdAt: new Date(),
      updatedAt: new Date()
    }
  })
  await queryInterface.bulkInsert('UserGameBiodatas',biodata , {})
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
  },

  async down (queryInterface, Sequelize) {
    return queryInterface.bulkDelete('UserGameBiodatas', null, {truncate: true, restartIdentity: true});
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
