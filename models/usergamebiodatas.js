'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class UserGameBiodatas extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      UserGameBiodatas.belongsTo(models.UserGames,{foreignKey:'userGameId'})
    }
  }
  UserGameBiodatas.init({
    userGameId: DataTypes.INTEGER,
    nickname: DataTypes.STRING,
    email: DataTypes.STRING,
    level: DataTypes.INTEGER,
    decription: DataTypes.TEXT
  }, {
    sequelize,
    modelName: 'UserGameBiodatas',
  });
  return UserGameBiodatas;
};