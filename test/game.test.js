const request = require('supertest')
const { sequelize } = require('../models/index')
const { queryInterface } = sequelize
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs')
const app = require('../app')

let jwttoken;

beforeEach(async () => {
    const salt = bcrypt.genSaltSync(10)
    const hash = bcrypt.hashSync("123asd", salt)
    await queryInterface.bulkInsert('Users', [
        {
            email: "opink@gmail.com",
            password: hash,
            createdAt: new Date(),
            updatedAt: new Date()
        }
    ])
    await queryInterface.bulkInsert('UserGames', [
        {
            username: 'opink1',
            password: '123test',
            createdAt: new Date(),
            updatedAt: new Date()

        }
    ])
    jwttoken = jwt.sign({
        id: 1,
        email: 'opink@gmail.com'
    }, 'token05')
})

afterEach(async () => {
    await queryInterface.bulkDelete('Users', {}, { truncate: true, restartIdentity: true })
    await queryInterface.bulkDelete('UserGames', {}, {truncate: true, restartIdentity: true});
})

describe('GET Games', () => {
    it('success', (done) => {
        request(app)
        .get('/games')
        .set('authorization', jwttoken)
        .end((err, res) => {
            if (err) {
            done(err)
            } else {
            expect(res.status).toBe(200)
            expect(Array.isArray(res.body)).toBe(true)
            done()
            }
        })
    })
    it('invalid token', (done) => {
        request(app)
        .get('/games')
        .set('authorization', 'token05')
        .end((err, res) => {
            if(err) {
                done(err)
            } else {
                expect(res.status).toBe(401)
                expect(res.body).toHaveProperty('message')
                expect(res.body.message).toBe('Unauthorized request')
                done()
            }
        })
    })
})

describe('POST Games', () => {
    it('success', (done) => {
        request(app)
        .post('/games')
        .set("authorization", jwttoken)
        .send({
            username: "opinks",
            password: "test1234"
        })
        .end((err, res) => {
            if (err) {
                done(err)
            } else {
                expect(res.status).toBe(200)
                expect(res.body).toHaveProperty('message')
                expect(res.body.message).toBe('Successfully create user')
                done()
            }
        })
    })
    it('No auth', (done) => {
        request(app)
        .post('/games')
        .send({
            username: "opink",
            password: "test123"
        })
        .end((err, res) => {
            if (err) {
                done(err)
            } else {
                expect(res.status).toBe(401)
                expect(res.body).toHaveProperty('message')
                expect(res.body.message).toBe('Unauthorized request')
                done()
            }
        })
    })
    it('Invalid auth token', (done) => {
        request(app)
        .post('/games')
        .set("authorization", "token05")
        .send({
            username: "opink",
            password: "test123"
        })
        .end((err, res) => {
            if (err) {
                done(err)
            } else {
                expect(res.status).toBe(401)
                expect(res.body).toHaveProperty('message')
                expect(res.body.message).toBe('Unauthorized request')
                done()
            }
        })
    })
})

describe('GET Games by ID', () => {
    it('success', (done) => {
        request(app)
        .get('/games/1')
        .set('authorization', jwttoken)
        .end((err, res) => {
            if (err) {
                done(err)
            } else {
                expect(res.status).toBe(200)
                expect(res.body).toHaveProperty('message')
                expect(res.body.message).toBe('Succesfully get data')
                done()
            }
        })
    })
    it('Invalid token', (done) => {
        request(app)
        .get('/games/1')
        .set('authorization', 'token05')
        .end((err, res) => {
            if (err) {
                done(err)
            } else {
                expect(res.status).toBe(401)
                expect(res.body).toHaveProperty('message')
                expect(res.body.message).toBe('Unauthorized request')
                done()
            }
        })
    })
    it('Not found', (done) => {
        request(app)
        .get('/games/2')
        .set('authorization', jwttoken)
        .end((err, res) => {
            if (err) {
            done(err)
            } else {
                expect(res.status).toBe(404)
                expect(res.body).toHaveProperty('message')
                expect(res.body.message).toBe('User not found')
                done()
            }
        })
    })
})

describe('UPDATE Games by ID', () => {
    it('Success', (done) => {
        request(app)
        .put('/games/1')
        .set('authorization', jwttoken)
        .send({
            "username": "opinkae",
            "password": "test123",
        })
        .end((err, res) => {
            if (err) {
                done(err)
            } else {
                expect(res.status).toBe(200)
                expect(res.body).toHaveProperty('message')
                expect(res.body.message).toBe('Successfully Update')
                done()
            }
        })
    })
    it('No Auth', (done) => {
        request(app)
        .put('/games/1')
        .send({
            "username": "opinkae",
            "password": "test123",
        })
        .end((err, res) => {
            if (err) {
                done(err)
            } else {
                expect(res.status).toBe(401)
                expect(res.body).toHaveProperty('message')
                expect(res.body.message).toBe('Unauthorized request')
                done()
            }
        })
    })
    it('Invalid Token', (done) => {
        request(app)
        .put('/games/1')
        .set('authorization', 'token05')
        .send({
            "username": "opinkae",
            "password": "test123",
        })
        .end((err, res) => {
            if (err) {
                done(err)
            } else {
                expect(res.status).toBe(401)
                expect(res.body).toHaveProperty('message')
                expect(res.body.message).toBe('Unauthorized request')
                done()
            }
        })
    })
})

describe('DELETE Games by ID', () => {

    it('Success', (done) => {
        request(app)
        .delete('/games/1')
        .set('authorization', jwttoken)
        .end((err, res) => {
            if (err) {
                done(err)
            } else {
                expect(res.status).toBe(200)
                expect(res.body).toHaveProperty('message')
                expect(res.body.message).toBe('Succesfully delete data')
                done()
            }
        })
    })
    it('Invalid token', (done) => {
        request(app)
        .delete('/games/1')
        .set('authorization', 'token05')
        .end((err, res) => {
            if (err) {
                done(err)
            } else {
                expect(res.status).toBe(401)
                expect(res.body).toHaveProperty('message')
                expect(res.body.message).toBe('Unauthorized request')
                done()
            }
        })
    })
    it('Not found', (done) => {
        request(app)
        .delete('/games/2')
        .set('authorization', jwttoken)
        .end((err, res) => {
            if (err) {
            done(err)
            } else {
                expect(res.status).toBe(404)
                expect(res.body).toHaveProperty('message')
                expect(res.body.message).toBe('Id not found')
                done()
            }
        })
    })
})