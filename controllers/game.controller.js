const { UserGames } = require('../models')
const cloudinary = require('../services/cloudinary.service')
const fs = require('fs')
class dataController {
    static async list(req, res, next){
        try {
        const usergames = await UserGames.findAll({
                attributes: ['id','username','password'],
            })
            res.status(200).json(usergames)
            }catch(err){
                next(err)
            }
    }

    static async getById(req, res, next){
        try {
            const usergames = await UserGames.findOne({
            where: {
                id: req.params.id
                }
            })
            if (!usergames) {
                throw {
                    status: 404,
                    message: 'User not found'
                }
            } else {
                res.status(200).json({
                    message: 'Succesfully get data'
                });
            }
        }catch(err){
                next(err)
            }
    }

    static async create(req, res, next) {
        try {
            const result = await cloudinary.uploader.upload(req.file.path,
                { resource_type: "video", 
                chunk_size: 6000000
                }
      )

            fs.unlinkSync(req.file.path)
            console.log(result)

            const usergames = await UserGames.create({
                email: req.body.email,
                password: req.body.password,
                upFile: result.secure_url
            })
            res.status(200).json({
                message:'Successfully create user'
            })
        }catch(err){
            next(err)
        }
    }

    static async update(req, res, next){
        try {
            const findId = await UserGames.findOne({
                where: {
                    id: req.params.id
                }
            })
            if (!findId){
                throw{
                    status : 404,
                    message : 'Id not found'
                }
            }else { 
                await UserGames.update({
                password: req.body.password
            },{
                where: {
                    id: req.params.id
                }
            })
            res.status(200).json({
                message: 'Successfully Update'
            })
        }
        }catch(err){
            next(err)
        } 
    }

    static async delete(req, res, next) {
        try {
            const findId = await UserGames.findOne({
                where: {
                    id: req.params.id
                }
            })
            if (!findId){
                throw{
                    status : 404,
                    message : 'Id not found'
                }
            }else {await UserGames.destroy({
            where: {
                id: req.params.id
            }
            })
            res.status(200).json({
                    message: 'Succesfully delete data'
                })
            }
            }catch(err) {
                next(err)
            }
    }
}

module.exports = dataController