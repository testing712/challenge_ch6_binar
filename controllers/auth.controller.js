const { UserGames } = require('../models')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const {OAuth2Client} = require('google-auth-library')
const client = new OAuth2Client(process.env.GOOGLE_CLIENT_ID, process.env.GOOGLE_CLIENT_SECRET)
const axios = require('axios')

class AuthController {
    static async register(req, res, next) {
        try {
            const salt = bcrypt.genSaltSync(10)
            const hash = bcrypt.hashSync(req.body.password, salt)
      
            const user = await UserGames.findOne({
                where: {
                    email: req.body.email
                }
            })

            await UserGames.create({
                email: req.body.email,
                password: hash
            })
      
            res.status(200).json({
                message: 'Successfully create user'
                })
        } catch (err) {
            next(err)
        }
    }
    static async login(req, res, next) {
        try {
            if (req.body.email && req.body.password) {

                const user = await UserGames.findOne({
                    where: {
                    email: req.body.email
                    }
                })
        
                if (!user) {
                    throw {
                    status: 401,
                    message: 'Invalid email or password'
                    }
                }
                if (bcrypt.compareSync(req.body.password, user.password)) {
                    const token = jwt.sign({
                    id: user.id,
                    email: user.email
                    }, 'token05')
        
                    res.status(200).json({
                    token
                    })
                } else {
                    throw {
                    status: 401,
                    message: 'Invalid email or password'
                    }
                }
            } else if (req.body.google_id_token) {
                const payload = await client.verifyIdToken ({
                    idToken: req.body.google_id_token,
                    audiance: process.env.GOOGLE_CLIENT_IDs
                })
                const user = await UserGames.findOne({
                    where: {
                        email: payload.payload.email
                        }
                    })
            
                    if (user) {
                        const token = jwt.sign({
                            id: user.id,
                            email: user.email
                        }, 'token05')
                        res.status(200).json({ token })
                    } else {
                        const createdUser = await UserGames.create({
                            email: payload.payload.email
                        })
                        const token = jwt.sign({
                            id: createdUser.id,
                            email: createdUser.email
                        }, 'token05')
                        res.status(200).json({ token })
                    }
                } else if (req.body.facebook_id_token) {
                const response = await axios.get(`https://graph.facebook.com/v12.0/me?fields=id%2Cname%2Cemail%2Cgender%2Cbirthday&access_token=${req.body.facebook_id_token}`)
            
                const user = await UserGames.findOne({
                    where: {
                    email: response.data.email
                    }
                })
                if (user) {
                    const token = jwt.sign({
                        id: user.id,
                        email: user.email
                    }, 'token05')
                    res.status(200).json({ token })
                } else {
                    const createdUser = await UserGames.create({
                        email: payload.payload.email
                    })
                    const token = jwt.sign({
                        id: createdUser.id,
                        email: createdUser.email
                    }, 'token05')
                    res.status(200).json({ token })
                }
            }
        } catch (err) {
        next(err)
        }
    }
}

module.exports = AuthController