const multer = require('multer')

module.exports = (err, req, res, next) => {
    console.log(err);
    if (err.status) {
        res.status(err.status).json({
            message: err.message,
        })
    } else if (err instanceof multer.MulterError === false) {
        res.status(400).json({
            message: 'File type not available'
        })
    } 
    else {
        req.sentry.captureException(err)
        res.status(500).json({
            message: 'Internal Server Error'
        })
    }
}
